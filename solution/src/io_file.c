#include "io_file.h"

static char *c_open_modes[] = {
        [O_RDONLY]            = "r",
        [O_WRONLY]            = "w",
        [O_RDWR]              = "r+",
        [O_WRONLY | O_APPEND] = "a",
        [O_RDWR | O_APPEND]   = "a+",
        [O_WRONLY | O_BINARY] = "wb",
        [O_RDONLY | O_BINARY] = "rb",
        [O_RDONLY | O_TEXT]   = "rt",
        [O_WRONLY | O_TEXT]   = "wt"
};

char *read_status_msg[] = {
        [READ_INVALID_FILE] = "Read error: invalid file",
        [READ_INVALID_TO_READ] = "Read error: invalid data destination",
        [READ_INVALID_SIGNATURE] = "Read error: invalid signature",
        [READ_INVALID_HEADER] = "Read error: invalid header"
};

char *write_status_msg[] = {
        [WRITE_INVALID_PATH] = "Write error: invalid path",
        [WRITE_INVALID_TO_WRITE] = "Write error: invalid target to write",
        [WRITE_ERROR] = "Writer error: failed write data"
};

FILE *open_file(const char *restrict filename, const enum open_mode mode) {
    return fopen(filename, c_open_modes[mode]);
}

enum read_status read_file(const char *restrict filename, file_reader *reader, void *to_write) {
    FILE *in = open_file(filename, O_RDONLY | O_BINARY);
    const enum read_status status = (in == NULL) ? READ_INVALID_FILE : reader(in, to_write);
    if (status != READ_OK) {
        fprintf(stderr, "%s\n", read_status_msg[status]);
    }
    fclose(in);
    return status;
}

enum write_status write_file(const char *restrict filename, file_writer *writer, const void *to_read) {
    FILE *out = open_file(filename, O_WRONLY | O_BINARY);
    const enum write_status status = (out == NULL) ? WRITE_INVALID_PATH : writer(out, to_read);
    if (status != WRITE_OK) {
        fprintf(stderr, "%s\n", write_status_msg[status]);
    }
    fclose(out);
    return status;
}
