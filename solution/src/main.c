#include "pe_file.h"
#include <stdio.h>

#define APP_NAME "section-extractor"

void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

int main(int argc, char **argv) {
    (void) argc;
    (void) argv;

    if (argc != 4) {
        usage(stdout);
        return 1;
    }

    const char *in_file      = argv[1];
    const char *section_name = argv[2];
    const char *out_file     = argv[3];

    struct PEFile pe_file;
    enum read_status pe_file_read_status = read_file(in_file, pe.file.read, &pe_file);
    if(pe_file_read_status != READ_OK) {
        return 2;
    }

    struct PESectionData section_data = {.header = pe.file.find_section(&pe_file, section_name)};
    enum read_status section_data_read_status = read_file(in_file, pe.section.data.read, &section_data);
    if(section_data_read_status != READ_OK) {
        return 3;
    }

    enum write_status section_data_write_status = write_file(out_file, pe.section.data.write, &section_data);
    if(section_data_write_status != WRITE_OK) {
        return 4;
    }


    pe.section.data.free(&section_data);
    pe.file.free(&pe_file);

    return 0;
}
