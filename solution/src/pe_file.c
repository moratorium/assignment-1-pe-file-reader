#include "pe_file.h"
#include <malloc.h>
#include <string.h>

#define IMAGE_DOS_SIGNATURE 0x5A4D
#define IMAGE_NT_SIGNATURE  0x00004550


void pe_file_free(struct PEFile const *pe_file) {
    free(pe_file->section_headers);
}

void pe_section_data_free(struct PESectionData const *pe_section_data) {
    free(pe_section_data->data);
}

struct PESectionHeader* pe_file_find_section(struct PEFile* pe_file, const char* name) {
    for(size_t i = 0; i < pe_file->header.number_of_sections; i++) {
        if(strcmp(pe_file->section_headers[i].name,name) == 0) {
            return pe_file->section_headers+i;
        }
    }

    return NULL;
}

static enum read_status pe_dos_header_read(FILE *restrict in, struct PEFile *pe_file) {
    if (fread(&pe_file->dos_header, sizeof(struct DOSHeader), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (pe_file->dos_header.magic != IMAGE_DOS_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;
}

static enum read_status pe_header_read(FILE *restrict in, struct PEFile *pe_file) {
    uint32_t magic;
    if (fread(&magic, sizeof(uint32_t), 1, in) != 1 || magic != IMAGE_NT_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (fread(&pe_file->header, sizeof(struct PEHeader), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum read_status pe_file_reader(FILE *restrict in, void *_pe_file) {
    if (_pe_file == NULL) {
        return READ_INVALID_TO_READ;
    }

    struct PEFile *pe_file = (struct PEFile *) _pe_file;

    const enum read_status dos_header_status = pe_dos_header_read(in, pe_file);
    if (dos_header_status != READ_OK) {
        return dos_header_status;
    }

    if (fseek(in, (long) pe_file->dos_header.lfanew, SEEK_SET)) {
        return READ_INVALID_SIGNATURE;
    }

    const enum read_status pe_header_status = pe_header_read(in, pe_file);
    if (pe_header_status != READ_OK) {
        return pe_header_status;
    }

    if (fseek(in, pe_file->header.size_of_optional_header, SEEK_CUR)) {
        return READ_INVALID_SIGNATURE;
    }

    pe_file->section_headers = malloc(sizeof(struct PESectionHeader) * pe_file->header.number_of_sections);

    if (fread(pe_file->section_headers, sizeof (struct PESectionHeader),pe_file->header.number_of_sections, in) != pe_file->header.number_of_sections)
        return READ_INVALID_SIGNATURE;

    return READ_OK;
}

enum read_status pe_section_data_reader(FILE *restrict in, void *_section_data) {
    struct PESectionData *section_data = (struct PESectionData*) _section_data;

    if(section_data == NULL || section_data->header == NULL) {
        return READ_INVALID_TO_READ;
    }

    const struct PESectionHeader *header = section_data->header;

    if (fseek(in, (long) header->pointer_to_raw_data, SEEK_SET)) {
        return READ_INVALID_SIGNATURE;
    }

    section_data->data = malloc(header->size_of_raw_data);
    if (fread(section_data->data, header->size_of_raw_data, 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum write_status pe_section_data_writer(FILE *restrict out, const void *_section_data) {
    struct PESectionData *section_data = (struct PESectionData*) _section_data;

    if (section_data == NULL || section_data->header == NULL) {
        return WRITE_INVALID_TO_WRITE;
    }

    fwrite(section_data->data, section_data->header->size_of_raw_data, 1, out);

    if (ferror(out) != 0) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
