#ifndef ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H
#define ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H

#include "io_file.h"
#include <stdint.h>


#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
PEHeader {
    uint16_t machine;
    uint16_t number_of_sections;
    uint32_t time_date_stamp;
    uint32_t pointer_to_symbol_table;
    uint32_t number_of_symbols;
    uint16_t size_of_optional_header;
    uint16_t characteristics;
};


struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
PESectionHeader {
    char  name[8];
    uint32_t virtual_size;
    uint32_t virtual_address;
    uint32_t size_of_raw_data;
    uint32_t pointer_to_raw_data;
    uint32_t pointer_to_relocations;
    uint32_t pointer_to_linenumbers;
    uint16_t number_of_relocations;
    uint16_t number_of_linenumbers;
    uint32_t characteristics;
};


struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
DOSHeader {
    uint16_t magic;
    uint16_t cblp;
    uint16_t cp;
    uint16_t crlc;
    uint16_t cparhdr;
    uint16_t minalloc;
    uint16_t maxalloc;
    uint16_t ss;
    uint16_t sp;
    uint16_t csum;
    uint16_t ip;
    uint16_t cs;
    uint16_t lfarlc;
    uint16_t ovno;
    uint16_t res[4];
    uint16_t oemid;
    uint16_t oeminfo;
    uint16_t res2[10];
    uint32_t lfanew;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

struct PEFile {
    struct DOSHeader dos_header;

    struct PEHeader header;

    struct PESectionHeader *section_headers;
};

struct PESectionData {
    struct PESectionHeader *header;

    uint8_t *data;
};


void pe_file_free(struct PEFile const *pe_file);

void pe_section_data_free(struct PESectionData const *pe_section_data);

struct PESectionHeader* pe_file_find_section(struct PEFile* pe_file, const char* name);

file_reader pe_file_reader;

file_reader pe_section_data_reader;

file_writer pe_section_data_writer;

static struct {
    struct {
        void (*free)(struct PEFile const *);
        file_reader* read;
        struct PESectionHeader* (*find_section)(struct PEFile* pe_file, const char* name);
    } file;
    struct {
        struct {
            file_writer *write;
            file_reader *read;
            void (*free)(struct PESectionData const *);
        } data;
    } section;
} const pe = {
        .file = {
                .free = pe_file_free,
                .read = pe_file_reader,
                .find_section = pe_file_find_section
        },
        .section = {
                .data = {
                    .write = pe_section_data_writer,
                    .read  = pe_section_data_reader,
                    .free  = pe_section_data_free
                }
        }
};

#endif //ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H
