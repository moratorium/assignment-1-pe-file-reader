/// @file
/// @brief Header file for higher-order io functions

#ifndef SECTION_EXTRACTOR_IO_FILE_H
#define SECTION_EXTRACTOR_IO_FILE_H

#include <stdio.h>


enum open_mode {
    O_RDONLY = 0,
    O_WRONLY = 1,
    O_RDWR = 2,
    O_APPEND = 4,
    O_BINARY = 8,
    O_TEXT = 16
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_TO_READ,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_PATH,
    WRITE_INVALID_TO_WRITE,
    WRITE_ERROR
};

typedef enum read_status(file_reader)(FILE *restrict in, void *input_file);

typedef enum write_status(file_writer)(FILE *restrict out, const void *output_file);

FILE *open_file(const char *restrict filename, enum open_mode mode);

enum read_status read_file(const char *filename, file_reader *reader, void *to_write);

enum write_status write_file(const char *filename, file_writer *writer, const void *to_read);

#endif //SECTION_EXTRACTOR_IO_FILE_H
